#Haystack Defs docs

## Steps to generate docs
####Required:
1] ssh://hg@bitbucket.org/project-haystack/haystack-defs

2] skyspark-3.0.20

3] Add absolute path of '\skyspark-3.0.20\bin' to Environment Variables

####Steps:
1] Compile def library
Go to haystack-defs/src and execute fan build.fan from command-line

2] A pod gets created in skyspark-3.0.20\lib\fan.

3] Go to skyspark-3.0.20 folder and Create skyspark docs using 
fan genDoc.fan

4] Docs gets created in skyspark-3.0.20\doc-def\

5] Open index.html for index of resultant docs