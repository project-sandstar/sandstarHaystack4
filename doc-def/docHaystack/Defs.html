<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>Defs &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Defs.html'>Defs</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Filters.html'>« Filters</a></li><li class='next'><a href='../docHaystack/Namespaces.html'>Namespaces »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>Defs</h1>

<h2 id='overview'>Overview</h2>

<p>Defs bind a symbolic name to a formal definition.  Defs are modeled as a dict with the <a href='../lib-ph/def.html'>def</a> tag.  Defs are used to define the tags we use on Haystack data.  And because they are dicts themselves, defs are normal Haystack data also.</p>

<p>The primary tags used to model a def include:</p>

<ul>
<li><a href='../lib-ph/def.html'>def</a>: binds a well-known name to the definition</li>

<li><a href='../lib-ph/doc.html'>doc</a>: human description of the definition</li>

<li><a href='../lib-ph/is.html'>is</a>: organizes the definition into a taxonomy type tree</li>

<li><a href='../lib-ph/lib.html'>lib</a>: specifies the library module which declares the definition</li>
</ul>

<p>See <a href='../lib-ph/def.html'>def tags</a> for the full list of all tags which might be used on a definition.</p>

<h2 id='layers'>Semantic Layers</h2>

<p>Defs allow us to build up layers of our information model:</p>

<ul>
<li><strong>No Defs</strong>: using dict as simple name/value pairs without formal defs</li>

<li><strong>Vocabulary</strong>: giving our tag names formal definitions as terms</li>

<li><strong>Taxonomy</strong>: organizing our terms into a subtype tree</li>

<li><strong>Ontology</strong>: modeling more complex relationships between definitions</li>
</ul>

<p>Lets start at the bottom: you can use the Haystack data model and file formats without any defs.  Even without defs, Haystack provides a nice framework for structuring and exchanging data.  It is like JSON but with a richer type system since we have first class types for dates, times, references, etc.</p>

<p>The real power of Haystack is unleashed when we use the def framework to formally define our tag names.  As a first step we create a def for each of our tag names (or conjuncts for sets of tag names).  This allows to us to precisely define our vocabulary.  Each of those tags now can be reflected to its definition.</p>

<p>We use the <code>is</code> tag in our defs to organize our meta-model into a taxonomy through <a href='../docHaystack/Subtyping.html'>subtyping</a>.  Subtyping allows us to relate terms as <a href='https://en.wikipedia.org/wiki/Hyponymy_and_hypernymy'>hyponyms and hypernyms</a>. For example we define that <a href='../lib-phScience/water.html'>water</a> is a subtype of <a href='../lib-phScience/liquid.html'>liquid</a>.  Subtyping allows us to infer semantics beyond the presence of tags.</p>

<p>Ontologies extend taxonomies to include the relationships between concepts. In Haystack we build out this ontological information using symbolic tags on defs to cross-reference other defs.  For example, we can model that a duct conveys air:</p>

<pre>def: ^duct
conveys: ^air</pre>

<p>Ontological relationships provide tremendous power to query the knowledge from Haystack data.</p>

<h2 id='types'>Def Types</h2>

<p>There are three types of definitions determined by the format of their symbol name:</p>

<ul>
<li><a href='../docHaystack/Defs.html#tags'>tag</a>: definition for a tag name</li>

<li><a href='../docHaystack/Defs.html#conjuncts'>conjunct</a>: definition for a set of tags such as <code>hot-water</code></li>

<li><a href='../docHaystack/Defs.html#keys'>key</a>: definition for a feature specific name such as <code>filetype:json</code></li>
</ul>

<p>Collectively we call tags and conjuncts our <a href='../docHaystack/Defs.html#terms'>terms</a>.</p>

<h2 id='tags'>Tag Defs</h2>

<p>Tag defs are used to formally define tag names.  They must have symbol names which are valid tag names:</p>

<ul>
<li>Must start with ASCII lower case letter (a-z)</li>

<li>Must contain only ASCII letters, digits, or underbar (a-z, A-Z, 0-9, _)</li>

<li>By convention use camel case (fooBarBaz)</li>
</ul>

<p>Here is an example:</p>

<pre>def: ^equip
doc: "Equipment asset"
is: ^entity</pre>

<h2 id='conjuncts'>Conjuncts</h2>

<p>Conjuncts are used to define a set of two or more tags used together in combination.  Conjuncts are like the compound words used in English and other languages.  We coin a new term from existing terms.</p>

<p>Conjunct symbols are formatted as individual names separated by a dash. Each tag name used in a conjunct must itself be formally defined and must be a subtype of marker.</p>

<p>Conjuncts are applied to dicts by applying each tag separately.  For example to apply the <a href='../lib-phIoT/elec-meter.html'>elec-meter</a> conjunct to a dict, we would add the individual tags <a href='../lib-phScience/elec.html'>elec</a> and <a href='../lib-phIoT/meter.html'>meter</a>.</p>

<p>The order of the names in a conjunct is significant - the symbol itself defines a unique identifier.  For example <code>hot-water</code> is <em>not</em> the same identifier as <code>water-hot</code>.  Note this is in contrast to instance data modeled as dicts which do not define any ordering on their tags.</p>

<p>The general rule for ordering the tags in a conjunct is to put the most important term or noun at the end.  For example since <code>hot</code> is an adjective on the noun <code>water</code>, then we order the conjunct as <code>hot-water</code>.  The order often maps to how we would use the phrase or compound word in normal language.</p>

<p>Conjuncts have no implied subtyping.  For example <a href='../lib-phIoT/hot-water.html'>hot-water</a> does not imply that it subtypes from either <a href='../lib-phIoT/hot.html'>hot</a> or <a href='../lib-phScience/water.html'>water</a>.  Subtyping must be explicitly specified in the def via the <code>is</code> tag.</p>

<p>Here is an example:</p>

<pre>def: ^hot-water
is: ^water
doc: "Hot water used for HVAC heating or supply to hot taps"</pre>

<h2 id='terms'>Terms</h2>

<p>Any def which is either a <em>tag</em> or a <em>conjunct</em> is a <em>term</em>.  Terms define our standardized vocabulary to model data.  It can be difficult to determine when a term should be a camel case tag or a conjunct.  For example should we use <code>hotWater</code> as a single tag or <code>hot-water</code> as a conjunct?</p>

<p>One consideration is how instance data will be queried with tags.  By making a term a conjunct, we can easily query the individual tags.  For example since <code>hot-water</code> is a conjunct, we can query data for the <code>water</code> tag which would include <code>hot-water</code>, <code>chilled-water</code>, etc.  Although in Haystack 4.0 we can also perform these queries using the subtype tree.</p>

<p>Another consideration is semantic conflicts.  Many of the primary entity tags carry very specific semantics.  For example the <a href='../lib-phIoT/site.html'>site</a> tag by its presence means the data models a geographic site.  So we cannot reuse the <code>site</code> tag to mean something associated with a site; which is why use the camel case tag <a href='../lib-phIoT/siteMeter.html'>siteMeter</a> to mean the main meter associated with a site.</p>

<h2 id='keys'>Feature Keys</h2>

<p>Feature keys create named definitions which are application specific.  Feature key symbols are formatted as <code>feature:name</code>. Currently we define two features:</p>

<ul>
<li><a href='../lib-ph/lib.html'>lib</a>: the namespace of library modules</li>

<li><a href='../lib-ph/filetype.html'>filetype</a>: the namespace of Haystack file format types</li>
</ul>

<p>Feature keys let us share a single unified symbolic namespace for all definitions.  But they don't pollute our tag namespace.  For example the symbol <a href='../lib-ph/filetype~json.html'>filetype:json</a> lets us define the JSON file format, but its wholly separate if we ever decide we want a <code>json</code> tag definition.</p>

<p>Feature keys require that the feature name be formally specified as a subtype of <a href='../lib-ph/feature.html'>feature</a>.  The name of the key must be a valid <a href='../docHaystack/Defs.html#tags'>tag name</a>. All the names within a feature key must be unique.</p>

<p>Feature keys are implied to subtype from their feature.  For example <a href='../lib-phIoT/index.html'>lib:phIoT</a> automatically subtypes from <a href='../lib-ph/lib.html'>lib</a>.  It is invalid to declare an <code>is</code> tag on a feature key def.</p>

<p>Feature keys are <a href='https://en.wikipedia.org/wiki/Singleton_pattern'>singletons</a>. They don't have data instances.  For example, there is no combination of tags applied to dicts to <a href='../docHaystack/Reflection.html'>implement</a> the <code>lib:ph</code> def.  The <code>lib:ph</code> def is itself the only instance.</p>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
