<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>Namespaces &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Namespaces.html'>Namespaces</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Defs.html'>« Defs</a></li><li class='next'><a href='../docHaystack/Subtyping.html'>Subtyping »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>Namespaces</h1>

<h2 id='overview'>Overview</h2>

<p>All defs are declared within a <em>lib</em> or library module.  A <em>namespace</em> is a set of libs which define a namespace of defs.  All applications using Haystack are performed within the context of a namespace.  Namespaces define the defs in scope for reflection, querying, and symbolic linking.</p>

<h2 id='libs'>Libs</h2>

<p>Libs or libraries are the fundamental unit of modularity in Project Haystack. Every def must be defined inside a parent lib.  The normalized representation of every def has a <a href='../lib-ph/lib.html'>lib</a> tag with a symbolic link to its parent library. However the <code>lib</code> tag itself is never declared explicitly, rather it is always implied during load time.</p>

<p>Libs are defined as a zip file with one or more <a href='../docHaystack/Trio.html'>Trio</a> files under the "lib/" directory.  There must be a file named "lib/lib.trio" which specifies exactly one dict for the lib's metadata.  The lib metadata should declare the following tags:</p>

<ul>
<li><a href='../lib-ph/def.html'>def</a>: must be a feature key under the <a href='../lib-ph/lib.html'>lib</a> feature</li>

<li><a href='../lib-ph/doc.html'>doc</a>: must provide a description of the library</li>

<li><a href='../lib-ph/version.html'>version</a>: identifies the version as series of integers separated by dot</li>

<li><a href='../lib-ph/baseUri.html'>baseUri</a>: an absolute URI used to map symbols to URIs (used by <a href='../docHaystack/Rdf.html'>Rdf</a>)</li>

<li><a href='../lib-ph/includes.html'>includes</a>: a list symbolic names for libs to import</li>
</ul>

<p>Any other Trio files under "lib/" contain the lib's defs.  These files may be named anything you like, but must have the ".trio" file extension (all lower case).</p>

<p>Here is an example for <a href='../lib-phIoT/index.html'>lib:phIoT</a>:</p>

<pre>def: ^lib:phIoT
doc: "Project Haystack definitions for Internet of Things"
version: "4.0.2"
baseUri: `https://project-haystack.org/def/phIoT/`
includes: [^lib:ph, ^lib:phScience]</pre>

<p>Lib name must be globally unique and should be chosen carefully. Project Haystack reserves lib names prefixed with "ph".</p>

<h2 id='phLibs'>Project Haystack Libs</h2>

<p>Project Haystack currently defines three standardized libs:</p>

<ul>
<li><a href='../lib-ph/index.html'>lib:ph</a>: core definitions</li>

<li><a href='../lib-phScience/index.html'>lib:phScience</a>: definitions related to scientific phenomenon and quantities</li>

<li><a href='../lib-phIoT/index.html'>lib:phIoT</a>: definitions related to domain of Internet of Things and the built environment</li>
</ul>

<p>Future Project Haystack lib names will be always be prefixed with "ph".</p>

<h2 id='lib'>Lib Namespaces</h2>

<p>Libs import external names into their local namespace via the <a href='../lib-ph/includes.html'>includes</a> tag.  All libs must include <code>lib:ph</code> (with the exception of <code>lib:ph</code> itself). The namespace of all included defs and locally defined defs in a library is called the <em>lib namespace</em>.</p>

<p>Only names found in the lib's namespace may be used for definition tag names or symbol values.  For example if a def needs to subtype from the <a href='../lib-phIoT/equip.html'>equip</a> tag, then its lib must declare an include on <a href='../lib-phIoT/index.html'>lib:phIoT</a> to bring <code>equip</code> into the namespace.  It is invalid to use a tag name or symbol value in a def which is not found in the lib's namespace.</p>

<h2 id='proj'>Project Namespaces</h2>

<p>Individual Haystack projects may pick and choose which libs are used to define their application specific data.  We call this def namespace the <em>project namespace</em>.  Project namespaces will often be a mix of standardized libs, vendor specific libs, and project specific libs.  It is outside the scope of this specification to define how libs are included or enabled for project namespaces.  However all project namespaces must have a mechanism to formally specify/export which libs are in scope.  This is best accomplished through the <code>libs</code> HTTP API operation (TODO).</p>

<h2 id='ns'>Symbolic Namespace</h2>

<p>A namespace is defined strictly by a list of libs and their corresponding children defs.  Thus we can logically model a namespace as a map of symbol names to defs.</p>

<p>Within a namespace, we use symbol names as simple unqualified names.  This works very much like mainstream programming languages.  For example in Java if you want to use <code>java.util.List</code> then you typically import the <code>java.util</code> package and then use the simple name <code>List</code> in your code. Likewise if you want to use the <a href='../lib-phIoT/equip.html'>equip</a> tag, then you include <a href='../lib-phIoT/index.html'>lib:phIoT</a> into your own lib and use the tag name <code>equip</code>.  We always use simple names in both defs and in our Haystack data.</p>

<p>So that begs the question: what if there is a naming conflict and two different libs declare the same symbol name?  Currently that is considered an error and those two libs cannot be used together.  As a pragmatic course of action, a def name should be globally unique to avoid conflicts.  With conjucts its fairly easy to coin unique symbol names when a term has different definitions.  This pattern is similiar to how Wikipedia uses a disambiguation term in parenthesis to maintain a single global namespace for articles.</p>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
