<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>RDF &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Rdf.html'>RDF</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Trio.html'>« Trio</a></li><li class='next'><a href='../docHaystack/Docs.html'>Docs »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>RDF</h1>

<h2 id='status'>Status</h2>

<p>The status of this document is "in-development". Mapping rules may change or be enhanced as a result of review.</p>

<h2 id='overview'>Overview</h2>

<p>Ontological relationships are conveyed in the Haystack information model using <a href='../docHaystack/Defs.html'>defs</a>. Haystack provides methods to query information about the relationships between defs, and to make inferences about these relationships. These defs are stored in a format, viz., Trio, that cannot be consumed by traditional semantic tools.</p>

<p>In order to make the Haystack ontology more accessible to users more familiar with standard tools for the semantic web, we define a mapping from Haystack defs to <a href='https://www.w3.org/RDF/'>RDF</a>. RDF is considered the "lingua franca" of the semantic web, so it is highly desirable to have a well-defined set of rules for exporting Haystack defs to RDF. We adopt the following high-level goals in defining this mapping:</p>

<ul>
<li>Export defs as RDF statements in <a href='https://www.w3.org/TR/turtle/'>Turtle</a> format.</li>

<li>Generate <a href='https://www.w3.org/TR/rdf-schema/'>RDFS</a> statements to add semantic meaning that is equivalent to the def.</li>

<li>Use <a href='https://www.w3.org/TR/owl2-primer/'>OWL</a> statements where they serve to increase the expressivity, and usability of the model.</li>
</ul>

<p>The rest of this document outlines a strategy for mapping Haystack defs to RDF.</p>

<h2 id='genRules'>General Mapping Rules</h2>

<p>These are the basic rules for mapping defs to RDF. They are further refined in the sections that follow.</p>

<p>The symbol for a def becomes the <strong>subject</strong> of an RDF statement.</p>

<p>Each tag/value pair on a def becomes the <strong>predicate</strong> and <strong>object</strong> of an RDF statement respectively.</p>

<ul>
<li><a href='../lib-ph/doc.html'>doc</a> tags are mapped to <code>rdfs:comment</code> predicates.</li>

<li>Values of the <a href='../lib-ph/is.html'>is</a> list become distinct <code>predicate/object</code> pairs</li>
</ul>

<p>RDF requires that every resource be uniquely identified by an <a href='https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier'>IRI</a>. Every Def is a logical resource, so we need to map def symbols to IRIs. To construct an IRI from a symbol:</p>

<ol style='list-style-type:decimal'>
<li>Obtain the <code>baseUri</code> from the <a href='../docHaystack/Namespaces.html#libs'>Lib</a> the def belongs to.</li>

<li>Append the version of the lib with an empty fragment</li>

<li>Append the symbol name</li>
</ol>

<p>For example, <a href='../lib-phIoT/site.html'>site</a> is defined in the <code>phIoT</code> library. That library has the following def:</p>

<pre>def: ^lib:phIoT
doc: "Project Haystack definitions for Internet of Things"
version: "4.0"
baseUri: `https://project-haystack.org/def/phIoT/`
includes: [^lib:ph, ^lib:phScience]
</pre>

<p>Therefore, the full IRI for the site def based on the above lib def is</p>

<pre>https://project-haystack.org/def/phIoT/4.0#site</pre>

<p>Consider the definition of a site:</p>

<pre>def: ^site
is: [^entity, ^geoPlace]
mandatory
doc: "Site is a geographic location of the built environment"
</pre>

<p>A minimal RDF mapping for this def would be:</p>

<pre>@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: &lt;http://www.w3.org/2001/XMLSchema#> .
@prefix owl: &lt;http://www.w3.org/2002/07/owl#> .
@prefix ph: &lt;https://project-haystack.org/def/ph/4.0#> .
@prefix phScience: &lt;https://project-haystack.org/def/phScience/4.0#> .
@prefix phIoT: &lt;https://project-haystack.org/def/phIoT/4.0#> .

phIoT:site is ph:entity, ph:geoPlace ;
    rdfs:comment "Site is a geographic location of the built environment" ;
    ph:mandatory ph:marker .
</pre>

<p>This is a valid RDF export, but it lacks the necessary statements to convey semantic and ontological meaning. The remaining sections refine the export rules for different def types to address this issue.</p>

<p><strong>NOTE</strong>: for the remaining examples I am omitting RDF <code>@prefix</code> statements for brevity.</p>

<h2 id='markers'>Marker Tags</h2>

<p>Defs for marker tags are subtypes of <a href='../lib-ph/marker.html'>marker</a> via the <a href='../lib-ph/is.html'>is</a> tag. See the section on <a href='../docHaystack/Subtyping.html'>Subtyping</a> for details.</p>

<p>Marker tag defs become instances of an <code>owl:Class</code>.</p>

<p>The supertype tree defined by <a href='../lib-ph/is.html'>is</a> maps to a set of <code>rdfs:subClassOf</code> statements. This establishes a subtyping relationship in the ontology.</p>

<p>Using these rules, the <a href='../lib-phIoT/site.html'>site</a> def now becomes</p>

<pre>phIoT:site a owl:Class ;
    rdfs:subClassOf ph:entity,
        ph:geoPlace ;
    rdfs:comment "Site is a geographic location of the built environment" ;
    ph:is ph:entity,
        ph:geoPlace ;
    ph:lib phIoT:lib:phIoT ;
    ph:mandatory ph:marker .
</pre>

<h2 id='datatypes'>Datatypes</h2>

<p>Haystack defines a number of data types (e.g. Str, Bool, DateTime). All data types in Haystack are subtypes of the <a href='../lib-ph/val.html'>val</a> def. Scalar data types (<a href='../lib-ph/scalar.html'>scalar</a>) are further refined as subtypes of <a href='../lib-ph/val.html'>val</a>. This section describes the rules for defining scalar data types in the RDF export.</p>

<p>Only <em>direct</em> subtypes of <a href='../lib-ph/scalar.html'>scalar</a> are defined as data types. They are declared as instances of the <code>owl:DatatypeProperty</code> class.</p>

<p><strong>Exception</strong>: marker tags are not defined as data types since they convey subtyping information (see <a href='../docHaystack/Rdf.html#markers'>Marker Tags</a> section).</p>

<p>We also declare scalar data types to be a subclass of the best XSD datatype using <code>rdfs:subClassOf</code>. The following rules map a Haystack scalar type to XSD data type:</p>

<ul>
<li><strong>bool</strong>: <code>xsd:boolean</code></li>

<li><strong>curVal</strong>: <code>rdfs:Literal</code></li>

<li><strong>date</strong>: <code>xsd:date</code></li>

<li><strong>dateTime</strong>: <code>xsd:dateTime</code></li>

<li><strong>number</strong>: <code>xsd:double</code></li>

<li><strong>ref</strong>: <code>xsd:anyURI</code></li>

<li><strong>symbol</strong>: <code>xsd:anyURI</code></li>

<li><strong>time</strong>: <code>xsd:time</code></li>

<li><strong>uri</strong>: <code>xsd:anyURI</code></li>

<li><strong>writeVal</strong>: <code>rdfs:Literal</code></li>

<li>All other scalar types are <code>xsd:string</code></li>
</ul>

<p>Example:</p>

<pre>ph:dateTime a owl:DatatypeProperty ;
    rdfs:subClassOf xsd:dateTime ;
    rdfs:comment "ISO 8601 timestamp followed by timezone identifier" ;
    ph:is ph:scalar ;
    ph:lib ph:lib:ph .

ph:number a owl:DatatypeProperty ;
    rdfs:subClassOf xsd:double ;
    rdfs:comment "Integer or floating point numbers annotated with an optional unit" ;
    ph:is ph:scalar ;
    ph:lib ph:lib:ph .
</pre>

<h2 id='valTags'>Value Tags</h2>

<p>A value tag def is any tag def that is <strong>not</strong> a subtype of <a href='../lib-ph/marker.html'>marker</a>.</p>

<p>If the tag def is a subtype of <a href='../lib-ph/ref.html'>ref</a> or is a <a href='../lib-ph/choice.html'>choice</a>, then declare the def to be an instance of <code>owl:ObjectProperty</code>. All other value tags are declared to be an instance of <code>owl:DatatypeProperty</code>.</p>

<p>If the tag def (or one of its <a href='../lib-ph/defx.html'>defx</a> extensions) declares a <a href='../lib-ph/tagOn.html'>tagOn</a> aspect, then specify the <code>rdfs:domain</code> to be all referent entities.</p>

<p>If the tag def is a <a href='../lib-ph/ref.html'>ref</a> or <a href='../lib-ph/choice.html'>choice</a>, then specify the <code>rdfs:range</code> to the value of the <a href='../lib-ph/of.html'>of</a> tag on the def.</p>

<p>If the def is a basic <a href='../docHaystack/Defs.html#tags'>Tag Def</a> (e.g. <a href='../lib-ph/tz.html'>tz</a>), then specify the <code>rdfs:range</code> to be the appropriate Haystack datatype (see <a href='../docHaystack/Rdf.html#datatypes'>Datatypes</a> section).</p>

<pre>// A Ref tag
phIoT:siteRef a owl:ObjectProperty ;
    rdfs:range phIoT:site ;
    rdfs:comment "Site which contains the entity" ;
    ph:is ph:ref ;
    ph:lib phIoT:lib:phIoT ;
    ph:of phIoT:site .

// A basic Tag Def with multiple domains
ph:tz a owl:DatatypeProperty ;
    rdfs:domain phIoT:site, phIoT:point ;
    rdfs:range ph:str ;
    rdfs:comment "Timezone identifier from standard timezone database" ;
    ph:is ph:str ;
    ph:lib ph:lib:ph .

// A choice aspect
phIoT:conveys a owl:ObjectProperty ;
  rdfs:range phScience:phenomenon ;
  rdfs:comment "Equipment conveys a substance or phenomenon." ;
  ph:is phIoT:equipFunction ;
  ph:lib phIoT:lib:phIoT ;
  ph:of phScience:phenomenon .
</pre>

<h2 id='instances'>Exporting Instances</h2>

<p>To this point we have been primarily interested in exporting Defs to RDF. However, we can also export instance data to RDF. In Haystack, an instance is a Dict with an <a href='../lib-ph/id.html'>id</a> tag. The following outlines the high-level rules for exporting instances (i.e. Dicts) to RDF.</p>

<ul>
<li>Instances should be blank nodes. If possible, the label for the blank node should be the id of the instance.</li>

<li>Use <code>rdf:type</code> (<code>"a"</code>) to indicate the entity is an instance of a particular <code>owl:Class</code>.</li>

<li>All marker tags should specified using <a href='hasTag'>hasTag</a></li>

<li>Encode tag values as defined in the <a href='../docHaystack/Rdf.html#datatypes'>data types</a> section.
<ul>
<li>Exception: If the tag is a Ref (e.g. <code>siteRef</code>) the value should be a blank node with a label that references the corresponding instance.</li>
</ul>
</li>

<li>If we don't have a def for a tag, do not export it to RDF.</li>
</ul>

<p>For example, here is the Trio for a site Dict:</p>

<pre>id:@24192ca1-0c85f75d "Headquarters"
site
area:140797ft²
tz:New_York
dis:Headquarters
geoAddr:"600 W Main St, Richmond, VA"
geoCoord:C(37.545826,-77.449188)
hq
metro:Richmond
primaryFunction:Office
yearBuilt:1999
</pre>

<p>And here is the RDF export of that site instance:</p>

<pre>_:24192ca1-0c85f75d
    a phIoT:site ;
    ph:hasTag site,
    phIoT:area 140797 ;
    ph:tz "New_York" ;
    ph:dis "Headquarters" ;
    ph:geoAddr "600 W Main St, Richmond, VA" ;
    ph:geoCoord "C(37.545826,-77.449188)" ;
    phIoT:primaryFunction "Office" ;
    phIoT:yearBuilt 1999 .
</pre>

<h2 id='todo'>Pending Work</h2>

<ul>
<li>How to handle the unit of numeric tags in instance exports?</li>

<li>How to indicate inverse relationships?</li>

<li>How to indicate transitive containment?</li>

<li>Can we leverage other OWL statements to improve the ontology?</li>
</ul>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
