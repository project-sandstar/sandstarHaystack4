<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>Reflection &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Reflection.html'>Reflection</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Normalization.html'>« Normalization</a></li><li class='next'><a href='../docHaystack/Relationships.html'>Relationships »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>Reflection</h1>

<h2 id='overview'>Overview</h2>

<p>Defs define our meta-model and dicts define our data instances. Haystack provides a standardized processes to map between the two:</p>

<ul>
<li><em>implementation</em>: mapping defs to a dict using the appropriate tags</li>

<li><em>reflection</em>: mapping a dict to its effective defs</li>
</ul>

<p>We say a dict <em>implements</em> a def when the it contains the appropriate tags.  For example if a dict defines the <a href='../lib-phIoT/equip.html'>equip</a> tag, then the dict implements the <a href='../lib-phIoT/equip.html'>equip</a> def.  If a dict contains both the <a href='../lib-phIoT/hot.html'>hot</a> and <a href='../lib-phScience/water.html'>water</a> tags then the dict implements <a href='../lib-phIoT/hot-water.html'>hot-water</a>.</p>

<p>Reflection is the inverse process.  We are given a dict and we compute the list of effective defs which the it implements.</p>

<p>Implementation and reflection in Haystack are a flavor of <a href='https://en.wikipedia.org/wiki/Structural_type_system'>structural typing</a>. Dict data never explicitly declares itself of a single class or type. Instead we use a combination of marker tags to indicate typing information. We use value tags to indicate an attribute or fact about the data.</p>

<h2 id='implementation'>Implementation</h2>

<p>The rules for a dict to implement a def are as follows:</p>

<ol style='list-style-type:decimal'>
<li>Based on the def type:
<ol style='list-style-type:lower-alpha'>
<li>Tag def: the single tag name is applied</li>

<li>Conjunct: each tag within the conjunct is applied</li>

<li>Feature keys: are never implemented</li>
</ol>
</li>

<li>We walk the supertype tree of the def and apply any tag which is marked as <a href='../lib-ph/mandatory.html'>mandatory</a></li>
</ol>

<p>Lets look at some examples.</p>

<p>If we want to apply the <a href='../lib-phIoT/point.html'>point</a> def to a dict, then we apply rule 1a.  There are no supertypes marked as <code>mandatory</code> so we are done.  Thus we apply only <code>{point}</code> to fully implement the <a href='../lib-phIoT/point.html'>point</a> def.</p>

<p>If we wish to apply the <a href='../lib-phIoT/building.html'>building</a> def, then we apply rule 1a.  However when we walk the supertype tree we see that there are two defs marked as <code>mandatory</code>: both <a href='../lib-phIoT/site.html'>site</a> and <a href='../lib-phIoT/space.html'>space</a>.  Therefore to implement <a href='../lib-phIoT/building.html'>building</a> requires adding all three tags: <code>{site, space, building}</code>.  As a general rule top-level entity markers such as <a href='../lib-phIoT/site.html'>site</a>, <a href='../lib-phIoT/space.html'>space</a>, <a href='../lib-phIoT/equip.html'>equip</a>, <a href='../lib-phIoT/point.html'>point</a> are always required. This makes it easy to query your tag based data.</p>

<p>If we wish to apply the <a href='../lib-phIoT/hot-water-plant.html'>hot-water-plant</a> def then we will apply rule 1b. Additionally this def subtypes from the mandatory <a href='../lib-phIoT/equip.html'>equip</a> tag.  So we need to add four tags <code>{hot, water, plant, equip}</code>.</p>

<h2 id='inheritance'>Inheritance</h2>

<p>When a dict implements a def, it implicitly implements all of that def's supertypes.  For example if we add the <a href='../lib-phScience/water.html'>water</a> tag to a dict, then we implicitly implement all of the supertypes of <a href='../lib-phScience/water.html'>water</a> which includes <a href='../lib-phScience/liquid.html'>liquid</a>, <a href='../lib-phScience/fluid.html'>fluid</a>, <a href='../lib-phScience/substance.html'>substance</a>, <a href='../lib-phScience/phenomenon.html'>phenomenon</a>, and <a href='../lib-ph/marker.html'>marker</a>.  We call this flattened list of supertypes the def's <em>inheritance</em>.</p>

<h2 id='reflection'>Reflection</h2>

<p>Reflection is the inverse of implementation.  Given a dict we compute the defs it implements.  Reflection is always done within the context of a <a href='../docHaystack/Namespaces.html'>namespace</a> to determine which defs are in scope.</p>

<p>To reflect a dict to its def, we walk each tag in the dict and apply the following rules:</p>

<ol style='list-style-type:decimal'>
<li>Check if the tag name maps to a tag def</li>

<li>If the tag maps to a possible conjunct, then check if the dict has all the conjunct's tags</li>

<li>Infer the <a href='../docHaystack/Reflection.html#inheritance'>inheritance</a> from all defs reflected from the previous steps</li>
</ol>

<p>Lets follow this process to reflect the following dict:</p>

<pre>id: @hwp, dis: "Hot Water Plant", hot, water, plant, equip</pre>

<p>Step 1 yield: <a href='../lib-ph/id.html'>id</a>, <a href='../lib-ph/dis.html'>dis</a>, <a href='../lib-phIoT/hot.html'>hot</a>, <a href='../lib-phScience/water.html'>water</a>, <a href='../lib-phIoT/plant.html'>plant</a>, <a href='../lib-phIoT/equip.html'>equip</a></p>

<p>Step 2 yields: <a href='../lib-phIoT/hot-water.html'>hot-water</a>, <a href='../lib-phIoT/hot-water-plant.html'>hot-water-plant</a></p>

<p>Step 3 yields:</p>

<ul>
<li>from hot-water-plant/equip: <a href='../lib-ph/entity.html'>entity</a>, <a href='../lib-ph/marker.html'>marker</a></li>

<li>from id/dis: <a href='../lib-ph/ref.html'>ref</a>, <a href='../lib-ph/str.html'>str</a>, <a href='../lib-ph/scalar.html'>scalar</a>, <a href='../lib-ph/val.html'>val</a></li>
</ul>

<h2 id='entityTypes'>Entity Types</h2>

<p>We use markers extensively in Haystack to indicate typing information.  However a marker tag is not a <em>type</em> per se.  In a tagging system we use tags to quickly and easily associate data to related terms.  Lets examine our example from above:</p>

<pre>id: @hwp, dis: "Hot Water Plant", hot, water, plant, equip</pre>

<p>This dict implements the term <a href='../lib-phIoT/hot-water.html'>hot-water</a>, but we would not say that the plant "is-a" a type of hot-water.  Those tags only mean that this data is related to hot-water.  This makes it convenient to query our data for things related to hot-water, water, or liquid.</p>

<p>However we would say that the dict above "is-a" type of <a href='../lib-phIoT/hot-water-plant.html'>hot-water-plant</a>.  We can reflect this information because <a href='../lib-phIoT/hot-water-plant.html'>hot-water-plant</a> is a subtype of <a href='../lib-ph/entity.html'>entity</a>. The <code>entity</code> subtype tree models primary typing information.  Terms which subtype from entity must be designed to stand on their own - they cannot be used in conjuncts.</p>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
