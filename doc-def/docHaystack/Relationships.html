<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>Relationships &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Relationships.html'>Relationships</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Reflection.html'>« Reflection</a></li><li class='next'><a href='../docHaystack/Zinc.html'>Zinc »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>Relationships</h1>

<h2 id='overview'>Overview</h2>

<p>Relationships enable us to model how spaces, equip, points, and processes are related to each other.  Almost all Haystack implementations will model the containment of physical spaces and equipment.  It is also typical to model the flows of energy and substances such as electricity, air, and water.</p>

<p class='NOTE'>NOTE: THIS DESIGN IS AN EARLY PROPOSAL</p>

<h2 id='entities'>Entity Relationships</h2>

<p>Relationships between entities are modeled by cross-referencing the <a href='../lib-ph/id.html'>id</a> identifier.  Lets look at how to establish a relationships between an AHU and the plant which supplies it hot water:</p>

<pre>// hot water plant entity
id: @hwp, hot, water, plant, equip

// AHU entity which receives hot water from the plant above
id: @ahu, ahu, equip, hotWaterPlantRef: @hwp</pre>

<p>The plant's primary identifier is modeled by the <a href='../lib-ph/id.html'>id</a> tag with a ref value of <code>@hwp</code>.  The id tag uniquely identifies this plant within the data set.  The second dict models an <a href='../lib-phIoT/ahu.html'>ahu</a> which has the reference tag <a href='../lib-phIoT/hotWaterPlantRef.html'>hotWaterPlantRef</a> referencing the plant by its id.  This model of cross-referencing is similar to the use of primary/foreign keys in a relational database.</p>

<h2 id='defs'>Def Relationships</h2>

<p>We use a similar model to create relationships between defs.  However instead of cross-referencing the <a href='../lib-ph/id.html'>id</a> tag, we use <a href='../lib-ph/def.html'>def</a> and its symbolic name.  For an example let us consider the def of <a href='../lib-phIoT/hotWaterPlantRef.html'>hotWaterPlantRef</a>:</p>

<pre>def: ^hotWaterPlantRef
is: ^ref
of: ^hot-water-plant
receives: ^hot-water
doc: "Plant which supplies hot water to the entity"</pre>

<p>The <a href='../lib-ph/def.html'>def</a> tag specifies the primary key for the definition which is a symbolic name.  We also have three other tags with a symbolic value:</p>

<ul>
<li><a href='../lib-ph/is.html'>is</a>: specifies a subtyping relationship</li>

<li><a href='../lib-ph/of.html'>of</a>: specifies the entity type this tag should reference</li>

<li><a href='../lib-ph/receives.html'>receives</a>: specifies the substance received by this relationship</li>
</ul>

<p>Relationships on the definition side provide the deep semantics used to infer knowledge from our entity relationships.</p>

<h2 id='querying'>Querying Relationships</h2>

<p>Project Haystack uses <a href='../docHaystack/Filters.html'>filters</a> as its simple declarative query language.  The most basic way to query relationships is by tags.  The following filter queries AHUs which receive hot water from our plant in the example above:</p>

<pre>ahu and hotWaterPlantRef == @hwp</pre>

<p>This filter would match any dict which has the <a href='../lib-phIoT/ahu.html'>ahu</a> tag and the <a href='../lib-phIoT/hotWaterPlantRef.html'>hotWaterPlantRef</a> tag with a value equal to the identifier <code>@hwp</code>.</p>

<p>This technique is also heavily used to match points associated with a given equipment.  For example to find the discharge temp sensor for the AHU identified as <code>@ahu</code> would be:</p>

<pre>discharge and temp and sensor and point and equipRef == @ahu</pre>

<p>We can use the <a href='../docHaystack/Filters.html#def'>def operator</a> to perform more abstract relationship queries.  This operator lets us use <a href='../docHaystack/Reflection.html'>reflection</a> and <a href='../docHaystack/Subtyping.html'>subtype</a> inference to construct queries which don't require detailed knowledge of how a system was tagged.  Using our plant example above we can query AHUs that receive anything:</p>

<pre>ahu and receives?</pre>

<p>This query matches dicts that have the <a href='../lib-phIoT/ahu.html'>ahu</a> tag and any reflected def with the <a href='../lib-ph/receives.html'>receives</a> tag.  The <code>?</code> operator performs indirection to query the tags on the defs.  For example:</p>

<pre>receives   // does the dict have a tag named "receives"
receives?  // does the dict implement a def with a tag name "receives"</pre>

<p>The rules to map a dict to its implemented defs is specified in the <a href='../docHaystack/Reflection.html#reflection'>reflection</a> chapter.</p>

<p>We can enhance our query to filter dicts which receive hot water like this:</p>

<pre>receives-hot-water?</pre>

<p>The term to the right of the first dash is used to match the value of the def's <a href='../lib-ph/receives.html'>receives</a> tag.  We can read the filter above as follows: does the dict implement a def with the <a href='../lib-ph/receives.html'>receives</a> tag where the value which fits <a href='../lib-phIoT/hot-water.html'>hot-water</a>.  Subtyping may be used too; any of the following filters would match our AHU also:</p>

<pre>receives-water?   // hot-water is a subtype of water
receives-liquid?  // hot-water is a subtype of liquid</pre>

<p>We can also give the <code>?</code> operator a value on the right hand side as follows:</p>

<pre>receives-hot-water? @hwp</pre>

<p>The filter above matches using the rules discussed with one new requirement: the tag's value must be equal to <code>@hwp</code>.  We can break down the steps as follows:</p>

<ol style='list-style-type:decimal'>
<li>Does the dict implement a def with <a href='../lib-ph/receives.html'>receives</a> (it does: <a href='../lib-phIoT/hotWaterPlantRef.html'>hotWaterPlantRef</a>)</li>

<li>Does the value of that tag <em>in the def</em> subtype from <a href='../lib-phIoT/hot-water.html'>hot-water</a> (it does)</li>

<li>Does the value of that tag <em>in the dict</em> equal <code>@hwp</code> (it does)</li>

<li>If all of the above are true, the overall filter is a match</li>
</ol>

<p>This syntax is fairly simple to use, but provides a lot of flexibility under the hood to integrate the ontology into your queries.</p>

<h2 id='transitive'>Transitive Relationships</h2>

<p>Relationship tags can be marked as <a href='../lib-ph/transitive.html'>transitive</a>.  A transitive relationship means that if the relationship applies between A and B and also between B and C, that it is inferred to apply between A and C.  Lets look at an example to illustrate:</p>

<pre>id: @ahu, ahu, equip
id: @fan, discharge, fan, equip, equipRef=@ahu
id: @status, speed, cmd, point, equipRef=@fan</pre>

<p>We have three entities in our example:</p>

<ul>
<li>AHU identified with <code>@ahu</code> id</li>

<li>Discharge fan contained by the AHU</li>

<li>Fan speed command point contained by the fan</li>
</ul>

<p>In this example, containment is modeled by <a href='../lib-phIoT/equipRef.html'>equipRef</a> which is tagged with the <a href='../lib-ph/containedBy.html'>containedBy</a> relationship. Furthermore the <a href='../lib-ph/containedBy.html'>containedBy</a> definition is tagged as <a href='../lib-ph/transitive.html'>transitive</a>.</p>

<p>Put it all together and we have a transitive containment relationship we can query.  The following filter will match both the fan equip <strong>and</strong> the speed point dicts:</p>

<pre>containedBy? @ahu</pre>

<p>The filter above will match any dict if it is contained by the given AHU either directly or indirectly.</p>

<p>This filter matches the fan equip as follows:</p>

<ol style='list-style-type:decimal'>
<li>Does the dict implement a def with the <code>containedBy</code> tag (it does: <a href='../lib-phIoT/equipRef.html'>equipRef</a>)</li>

<li>Is the value of that tag in the dict equal to <code>@ahu</code> (yeap)</li>

<li>Match</li>
</ol>

<p>The filter matches the fan speed point as follows</p>

<ol style='list-style-type:decimal'>
<li>Does the dict implement a def with the <code>containedBy</code> tag (it does: <a href='../lib-phIoT/equipRef.html'>equipRef</a>)</li>

<li>Does the value of that tag in the dict equal <code>@ahu</code> (it does not, its value is <code>@fan</code>)</li>

<li>Is the def marked as transitive (yes, containedBy is transitive)</li>

<li>If so, then does the entity referenced the by tag (<code>@fan</code> in this case) itself match the containedBy filter? It does because of the previous steps we evaluated</li>
</ol>

<p>Transitive relationships don't require the same reference tag is used.  For example we might traverse a <a href='../lib-phIoT/equipRef.html'>equipRef</a> first, and a <a href='../lib-phIoT/spaceRef.html'>spaceRef</a> tag second. What matters that there is a recursive set of matching relationships.</p>

<h2 id='inverse'>Inverse Relationships</h2>

<p>Its also desirable to query relationships from either endpoint without regard to which endpoint actually declares the reference tag.  Using our plant example we want to be able to query both sides as follows:</p>

<pre>receives-hot-water? @hwp  // receives hot water from the plant
supplies-hot-water? @ahu  // supplies hot water to the AHU</pre>

<p>We call these <em>inverse</em> relationships.  They are explicitly configured on the relationship defs via the <a href='../lib-ph/inverseOf.html'>inverseOf</a> tag.</p>

<p>Lets follow the process for this filter which should match the <code>@hwp</code> entity:</p>

<pre>supplies-hot-water? @ahu</pre>

<p>Following the rules we have discussed above so far, we would not find a match.  There is no tag on the plant that directly references the AHU. However because <a href='../lib-ph/supplies.html'>supplies</a> specifies it is an inverse of <a href='../lib-ph/receives.html'>receives</a>, we would then have to check for a match in the reverse direction.  The inverse is constructed as follows when matching the <code>@hwp</code> entity:</p>

<ol style='list-style-type:decimal'>
<li>Replace <code>supplies</code> with its inverse which is <code>receives</code></li>

<li>Leave the subject term to the right of the first dash</li>

<li>Replace the reference value <code>@ahu</code> with the dict being processed which is <code>@hwp</code></li>

<li>Replace the dict being processed with entity identified by <code>@ahu</code></li>
</ol>

<p>Following those rules we would end up with an inverse filter which looks just the one we have already examined and we know will match:</p>

<pre>receives-hot-water? @hwp</pre>

<p>Together transitive and inverse relationships provide tremendous flexibility to query Haystack data without regard to the underlying reference tags used.</p>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
