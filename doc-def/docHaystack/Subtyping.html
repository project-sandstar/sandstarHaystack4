<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta charset='UTF-8'/><title>Subtyping &ndash; Project Haystack</title>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body style='padding:0; background:#fff; margin:1em 4em 3em 6em;'>
<div class='defc'>

<div class='defc-nav'>
<ul>
<li><a href='../index.html'>Index</a></li><li> » </li><li><a href='../docHaystack/index.html'>docHaystack</a></li><li> » </li><li><a href='../docHaystack/Subtyping.html'>Subtyping</a></li></ul>
</div>
<hr />

<div class='defc-nav'>
<ul>
<li class='prev'><a href='../docHaystack/Namespaces.html'>« Namespaces</a></li><li class='next'><a href='../docHaystack/Normalization.html'>Normalization »</a></li></ul>
</div>
<div class='defc-main'>

</div>
<div class='defc-manual'>
<h1 class='defc-chapter-title'>Subtyping</h1>

<h2 id='overview'>Overview</h2>

<p>Defs are organized into a tree structured taxonomy via the <a href='../lib-ph/is.html'>is</a> tag. A def is a <em>subtype</em> if it defines a subset or narrowing of a broader term.  For example we say that <a href='../lib-phScience/water.html'>water</a> is a subtype of <a href='../lib-phScience/liquid.html'>liquid</a> because it is a specific type of liquid.  The converse is that liquid is a <em>supertype</em> of water.</p>

<p>In the context of natural language, a subtype is a <a href='https://en.wiktionary.org/wiki/hyponym'>hyponym</a> and a supertype is a <a href='https://en.wiktionary.org/wiki/hypernym'>hypernym</a>. If you have ever played Mad Libs, then subtyping is intuitively like filling in the blanks.  For example if we say that a pipe conveys a <em>fluid</em>, then the term <em>fluid</em> encompasses specific types of fluids including water, steam, gasoline, or fuel oil.</p>

<p>We can also evaluate subtyping in the context of set theory.  If we say that <a href='../lib-phIoT/elec-meter.html'>elec-meter</a> is a subtype of <a href='../lib-phIoT/meter.html'>meter</a>, then we are saying the set of all things that are elec meters is wholly contained by the set of all meters.</p>

<p>Subtyping is a powerful knowledge modeling tool.  However remember that modeling the real world is a messy business.  Its impossible to design a taxonomy that fits all situations.  Consider the platypus and its difficulty to fit into a biological taxonomy.  Haystack attempts to define a pragmatic approach between simplicity and the common use cases.  But you will likely find use cases which aren't a perfect fit for Haystack's ontology.</p>

<h2 id='transitivity'>Transitivity</h2>

<p>Subtyping is defined to be a transitive relationship.  If <code>B</code> is a subtype of <code>A</code> and <code>C</code> is a subtype of <code>B</code>, then it is implied that <code>C</code> is a subtype of <code>A</code>.  A concrete example: <a href='../lib-phScience/water.html'>water</a> is a subtype of <a href='../lib-phScience/liquid.html'>liquid</a> and <a href='../lib-phScience/liquid.html'>liquid</a> is a subtype of <a href='../lib-phScience/fluid.html'>fluid</a>, therefore <a href='../lib-phScience/water.html'>water</a> is inferred to be a subtype of <a href='../lib-phScience/fluid.html'>fluid</a>.</p>

<h2 id='is'>Is Tag</h2>

<p>Subtyping is declared via the <a href='../lib-ph/is.html'>is</a> tag on a def.  The value of the <code>is</code> tag is a symbol or list of symbols.  Since most defs subtype from a single def, we typically use just a symbol:</p>

<pre>def: ^water
is: ^liquid</pre>

<p>In cases where multiple supertypes are required, use a list of symbols:</p>

<pre>def: ^building
is: [^site, ^space]</pre>

<p>All terms are required to have an <code>is</code> tag with the exception of the <em>root terms</em> which are <a href='../lib-ph/marker.html'>marker</a>, <a href='../lib-ph/val.html'>val</a>, <a href='../lib-ph/aspect.html'>aspect</a>, and <a href='../lib-ph/feature.html'>feature</a>.  <a href='../docHaystack/Defs.html#keys'>Feature keys</a> have implied subtyping rules and must not declare an <code>is</code> tag.</p>

<p>It is invalid to create cyclic subtyping relationships.  Subtyping must result in a strict tree structure.</p>

<h2 id='inheritance'>Inheritance</h2>

<p>Defs are themselves specified a set of tag name/value pairs.  Subtypes automatically inherit these tags into their own definitions.  We call this process <em>inheritance</em> and it works similar to inheritance in traditional object-oriented languages.</p>

<p>We call the tags explicitly defined in a def the <em>declared</em> tags.  The tags that are effective after inheritance is applied are called the <em>normalized</em> tags.  Normalization rules are as follows:</p>

<ol style='list-style-type:decimal'>
<li>Start with the declared types from the def</li>

<li>Each supertype is processed in order of declaration in the <code>is</code> tag</li>

<li>The supertype must be fully normalized itself</li>

<li>Filter out any tags from the supertype marked as <a href='../lib-ph/notInherited.html'>notInherited</a></li>

<li>Inherit all tags from step 3 which are not yet declared or inherited from other supertypes</li>
</ol>

<p>In the case of ambiguity from multiple inheritance, a subtype should explicitly declare the tag value.</p>

<p>Consider this completely fictional example for an <a href='https://en.wikipedia.org/wiki/Chevrolet_El_Camino'>El Camino</a> which is a hybrid between a car and a pickup truck:</p>

<pre>// declarations
def: ^car
numDoors: 4
color: "red"
engine: "V8"
----
def: ^pickupTruck
numDoors: 2
color: "blue"
bedLength: 80in
----
def: ^elCamino
is: [^pickup, ^car]
color: "purple"</pre>

<p>The normalized definition with inheritance would be:</p>

<pre>def: ^elCamino        // declared
is: [^pickup, ^car]   // declared
color: "purple"       // declared
numDoors: 2           // inherited from pickup first
engine: "V8"          // inherited from car
bedLength: 80in       // inherited from pickup</pre>
<h2 class='defc-main-heading'></h2>
<div class='defc-main-section'>
<p class='defc-footer-ts'>
Generated 11-Jun-2019 Tue 7:33:36AM IST</p>

</div>

</div>
</div>
</body>
</html>
