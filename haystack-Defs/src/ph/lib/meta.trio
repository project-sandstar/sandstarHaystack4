//
// Copyright (c) 2011, Project-Haystack
// Licensed under the Academic Free License version 3.0
//
// History:
//   22 Feb 2011  Brian Frank  Creation
//    6 Jan 2019  Brian Frank  New design
//

--------------------------------------------------------------------------
def: ^baseUri
is: ^uri
tagOn: ^lib
doc: "Base URI for normalizing relative URIs"
--------------------------------------------------------------------------
def: ^children
is: ^synthesizer
doc: "Types contained by this entity"
--------------------------------------------------------------------------
def: ^computed
is: ^marker
doc:
  Indicates a definition which is computed.  Computed defs cannot be
  used as tags themselves.
--------------------------------------------------------------------------
def: ^def
is: ^symbol
doc: "Create a new definition bound to the given symbol"
--------------------------------------------------------------------------
def: ^defx
is: ^symbol
doc: "Extends the given definition with additional meta tags"
--------------------------------------------------------------------------
def: ^doc
is: ^str
tagOn: ^def
doc:
  Documentation in simplified flavor of markdown.  The first
  sentence up to the period is used as the summary.

  Specific formatting options:
  pre>
  // inline formatting
  *italic*           // italics font
  **bold**           // bold font
  'code'             // code or API term
  `point`            // hyperlink to def (code link)
  [text]`point`      // hyperlink with explicit link text
  ![alt]`image.png`  // image

  // unordered list
  - one
  - two
  - three

  // ordered list; use numbers or letters
  1. one
  2. two
  3. three
  <pre

  The main difference between markdown is that inline code uses single
  ticks and hyperlinks are annotated using the backtick.  Links may be:
    - absolute http/https URIs
    - def symbol such as "site" or "hot-water"
--------------------------------------------------------------------------
def: ^docSection
is: ^marker
doc: "This tag is documented with its own section."
--------------------------------------------------------------------------
def: ^docTaxonomy
is: ^marker
notInherited
doc:
  Generate a taxonomy tree for this term in the documentation index.  If
  applied to an `entity` subtype then the link will be listed under the
  [entities]`docHaystack::Docs#entities` section, otherwise its listed
  in [taxonomies]`docHaystack::Docs#taxonomies` section.
--------------------------------------------------------------------------
def: ^enum
is: ^str
doc:
  Defines an eumeration of string keys.  The range may also be applied
  to format a Bool ordered as "false,true".  The string may be formatted
  in one of three ways:
   - comma separated keys on one line
   - keys separated by a "\n" newline character
   - markdown unordered list formatted as a series of "- key: description\n"
--------------------------------------------------------------------------
def: ^feature
docTaxonomy
doc: "Feature namespace of definitions formatted as 'feature:name'"
--------------------------------------------------------------------------
def: ^lib
is: ^feature
doc: "Library module of symbolic definitions"
--------------------------------------------------------------------------
def: ^includes
is: ^list
of: ^ref
tagOn: ^lib
doc: "List of library names to include"
--------------------------------------------------------------------------
def: ^mandatory
is: ^marker
tagOn: ^def
notInherited
doc:
  Requires that the marker be applied to dicts which use the marker's subtypes.
  This tag is typically applied to top-level entity tags such as 'equip'.
  For example if an instance is tagged with a subtype of 'equip' such
  as 'tank', then the instance must also include the 'equip' tag as a
  mandatory marker.
--------------------------------------------------------------------------
def: ^notInherited
is: ^marker
tagOn: ^def
doc:
  Marker applied to a def to indicate that is not inherited into
  subtype definitions.
--------------------------------------------------------------------------
def: ^synthesizer
is: ^str
doc: "Synthesizes defs for documentation and tooling"
--------------------------------------------------------------------------
def: ^synthetic
is: ^marker
doc: "Automatically generated"
--------------------------------------------------------------------------
def: ^transient
is: ^marker
tagOn: ^def
doc:
  Indicates a value tag which should not be persisted.  Transient
  values often change frequently and do not provide long term informational
  value. Typyically they are stored only in RAM and should not be stored
  to durable storage.
--------------------------------------------------------------------------
def: ^version
is: ^str
tagOn: ^lib
doc: "Version string formatted as decimal integers separated by a dot"
--------------------------------------------------------------------------
def: ^wikipedia
is: ^uri
tagOn: ^def
doc: "Hyperlink to the subject's page on Wikipedia"
--------------------------------------------------------------------------



