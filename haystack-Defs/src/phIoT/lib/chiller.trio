//
// Copyright (c) 2019, Project-Haystack
// Licensed under the Academic Free License version 3.0
//
// History:
//   21 Jan 2019  Brian Frank  Creation
//

--------------------------------------------------------------------------
def: ^chiller
is: ^equip
cools: ^chilled-water
chillerMechanism: ^chillerMechanismType
wikipedia: `https://en.wikipedia.org/wiki/Chiller`
doc:
  Equipment to remove heat from a liquid.  Chillers typically use a vapor
  compression or an absorption refrigeration cycle.
children:
  state-run-cmd
  state-run-sensor
  state-enable-cmd
  chilled-water-leaving-pipe
  chilled-water-entering-pipe
  condenser-water-leaving-pipe
  condenser-water-entering-pipe
--------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////
// Tags
//////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------
def: ^coolingCapacity
is: ^number
tagOn: ^chiller
doc: "Measurement of a chiller ability to remove heat measured"
prefUnit: ["tonref", "BTU/h", "kW"]
--------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////
// chillerMechanism
//////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------
def: ^chillerMechanism
is: ^choice
of: ^chillerMechanismType
doc: "Primary mechanism of chiller"
--------------------------------------------------------------------------
def: ^chillerMechanismType
is: ^marker
doc: "Primary mechanism of chiller"
--------------------------------------------------------------------------
def: ^chiller-absorption
is: [^absorption, ^chillerMechanismType]
--------------------------------------------------------------------------
def: ^chiller-reciprocal
is: [^reciprocal, ^chillerMechanismType]
--------------------------------------------------------------------------
def: ^chiller-rotaryScrew
is: [^rotaryScrew, ^chillerMechanismType]
--------------------------------------------------------------------------
def: ^chiller-centrifugal
is: [^centrifugal, ^chillerMechanismType]
--------------------------------------------------------------------------


