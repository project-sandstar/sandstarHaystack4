#! /usr/bin/env fan
//
// Copyright (c) 2019, Project-Sandstar
// Licensed under the Academic Free License version 3.0
//
// History:
//   28 May 2019  Kushal Dalsania  Creation
//

using build

**
** Build: phSandstar
**
class Build : BuildPod
{
  new make()
  {
    podName = "phSandstar"
    summary = "Project Haystack definitions for Sandstar"
    meta    = ["org.name":     "Project Haystack",
               "org.uri":      "https://project-haystack.org/",
               "proj.name":    "Project Haystack Defs",
               "license.name": "Academic Free License 3.0"]
    resDirs = [`lib/`]
  }
}