using defc

class Main {
    Void main (){
        c := DefCompiler()
        c.outDir = Env.cur.workDir + `doc-def/`
        c.inputs.add(CompilerInput.makePodName("phSandstar"))
        c.compileDocs
    }
}
